require "gitlab"

load(File.expand_path(File.dirname(__FILE__)+"/../salsarc"))
#SALSA_TOKEN = ENV["GITLAB_API_PRIVATE_TOKEN"] || (File.exists?("salsa.token") && File.read("salsa.token").strip)
SALSA_NAMESPACE=2159 # ruby-team group
SALSA_GROUP="ruby-team"
SALSA_URL="https://salsa.debian.org/api/v4"

Gitlab.endpoint=SALSA_URL
Gitlab.private_token=SALSA_TOKEN

def create_project(name)
  begin
    project=Gitlab.create_project(name, {description: "#{name} packaging", namespace_id: SALSA_NAMESPACE, visibility: "public"})
    puts "I #{name}: repository created (id: #{project.id})"
    return project
  rescue Gitlab::Error::Error => error
    puts error
    puts "E #{name}: repository creation failed. Probably already exists."
    begin
      project=Gitlab.project([SALSA_GROUP,name].join("/"))
    rescue Gitlab::Error::Error => error
      puts error
      puts "E #{name}: repository not found. Stopping here."
      exit(1)
    end
  end
end

def set_irker_notifications(project)
  Gitlab.change_service(project.id, :irker,
                          "recipients=debian-ruby&default_irc_uri=irc://irc.oftc.net:6667/&server_host=ruprecht.snow-crash.org&server_port=6659&colorize_messages=true")
  puts "I #{project.name}: irker notification activated"
end
def unset_irker_notifications(project)
  Gitlab.delete_service(project.id, :irker)
  puts "I #{project.name}: irker notification deactivated"
end

def set_webhooks(project)
  set_tagpending(project)
  set_kgb(project)
end

def set_tagpending(project)
  #tagpending
  tagpending_url="https://webhook.salsa.debian.org/tagpending/#{project.name}"
  if Gitlab.project_hooks(project.id).all? {|hook| hook.url != tagpending_url}
    Gitlab.add_project_hook(project.id, tagpending_url, {push_events: true})
    puts "I #{project.name}: tagpending webhook activated"
  else
    puts "W #{project.name}: tagpending webhook already enabled"
  end
end

def set_kgb(project)
  # KGB irc notifications
  kgb_url="http://kgb.debian.net:9418/webhook/?channel=debian-ruby"
  if Gitlab.project_hooks(project.id).all? {|hook| hook.url != kgb_url}
    Gitlab.add_project_hook(project.id, kgb_url,
                            {issues_events: true,
                             push_events: true,
                             merge_requests_events: true,
                             tag_push_events: true,
                             note_events: true,
                             job_events: true,
                             pipe_events: true,
                             wiki_page_events: true})
    puts "I #{project.name}: kgb webhook activated"
  else
    puts "W #{project.name}: kgb webhook already enabled"
  end
end

def team_projects
  Gitlab.group(SALSA_NAMESPACE).projects
end
